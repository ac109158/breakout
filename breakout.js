
var row = 0; 
var col = 0
var width = 10;
var height = 10;
var gameWidth = 800;
var paddleHeight;
var paddleWidth = 140;
var ballWidth = 30;
var ballHeight = 30;
var offset = 70;
var leftEdge = 0;
var rightEdge = leftEdge + gameWidth - paddleWidth;
// ball velocity
var msPerFrame = 20;
var secondsPerFrame = msPerFrame / 1000;

// This sets horizontal rate to 200--600 pixels/second
var vx = secondsPerFrame * (Math.floor(Math.random() * 400) + 200);
var defaultXVelocity = vx;
if (Math.random() < 0.5) vx = -vx;

// This sets verical rate to 500 pixels/second
var vy = secondsPerFrame * 500;
var defaultYVelocity = vx;
var speed1 = false;
var speed2 = false;
var speed3 = false;
var speed4 = false;

var defaultColor = 'black';
var paddleColor = 'black'

var broken = 0;
var brokenBricks = new Array();


function isPaddleCollission()
{
	var ball = $('#ball').position();
	var paddle = $('#paddle').position();
	//if the left edge of the ball is to the right of the right edge of the paddle, there is no collision.
	if ( ball.left > paddle.left + paddleWidth) 
	{
		// no paddle collision
		return false;
	}
	//If the the right edge of the ball is left of the left edge of the paddle, there is no collision.
	if ( ball.left + ballWidth < paddle.left) 
	{
		// no paddle collision
		return false;
	}
	//If the bottom of the ball is above the top of the paddle, no collision.
	if ( ball.top + ballHeight < paddle.top ) 
	{
		// no paddle collision
		return false;
	}
	//If the top of the ball is below the bottom of the paddle, no collision
	if ( ball.top > paddle.top + paddleHeight ) 
	{
		// no paddle collision
		return false;
	}
	else
	{
		//collision with paddle
		if ( Math.abs(ball.left  - (paddle.left + paddleWidth)) < 20 &&  Math.abs((ball.top + ballHeight) - paddle.top) < 20 )
		{
			vx = Math.abs(vx *1.5);
			$('#ball').css('background-color', 'red'); // if the ball hits a  right corner it rages to the right
			$('#paddle').removeClass('leftcornerhit').addClass('rightcornerhit');
			$('#paddle').width(paddleWidth-10);
		}
		else if ( Math.abs((ball.left + ballWidth) -paddle.left) < 20  &&  Math.abs((ball.top + ballHeight ) - paddle.top) <20 )
		{
			vx = -(Math.abs(vx) *1.5);
			$('#ball').css('background-color', 'red'); // if the ball hits a  left corner it rages to the left
			$('#paddle').removeClass('rightcornerhit').addClass('leftcornerhit');
			$('#paddle').width(paddleWidth-10);
		}
		else 
		{
			$('#ball').css('background-color', defaultColor);
			$('#paddle').removeClass('rightcornerhit').removeClass('leftcornerhit');
			paddleWidth = 140;
			offset = 70;
			$('#paddle').width(paddleWidth);
			if (vx < 0)
			{
				vx = -(defaultXVelocity);
				vy = defaultYVelocity;

			}
			else
			{
				vx = defaultXVelocity;
				vy = defaultYVelocity;

			}
		}
	

		return true;
	}
}

function moveBall()
	{

		var ball = $('#ball').position();
		bx = ball.left;
		by = ball.top;
		bx = bx + (.8*vx); 
		by = by - (.8*vy);
		if (bx <= 0)
		{
			bx = 1;
			vx *=-1;
		}
		else if (bx >= 800 -ballWidth)
		{
			bx = 800 -ballWidth -1;
			vx *=-1;
		}
		if (by <= 0)
		{
			by = 1;
			vy *=-1;
		}
		else if (by >= 590)
		{
			by = 588;
			vy *=-1;
		}
		$('#ball').css('left', bx);
		$('#ball').css('top', by);
		if (by >= 560)
		{
			window.location = "breakout.html";
		}
		if (isPaddleCollission()) 
		{
		vy = Math.abs(vy);

		}
		isBrickCollission(bx, by);
		isBrickCollission(bx+ballWidth, by);
		isBrickCollission(bx + ballWidth, by+ballWidth);
		isBrickCollission(bx, by+ballWidth);
		if ( broken == 100)
		{
			alert ("Winner");
    			window.location = "breakout.html";
		}
		setTimeout(moveBall, 20);
	}

function isBrickCollission(x,y)
{
	var brickx = 80;  // dimensions of a brick
	var bricky = 20;
	var row = Math.floor((y - 100) / bricky);
	var col = Math.floor(x / brickx);
	if (row < 0 || row >= 10 || col < 0 || col >= 10)
	{
		// not in the right area	
	}
	
	else if ((x+2) % brickx < 4 || (y+2) % bricky < 4)
	{
		// not quite in the brick--it's in the white border around a brick	
	}
	else 
	{
		// otherwise, row and column give the brick number
		var key = String(col) + ',' + String(row);
		if (key in brokenBricks) 
		{
			// brick is already broken, pretend it's not there
		} 
		else 
			{
			// brick has not been broken yet
			brokenBricks[key] = true; // now it has
			broken++;
			// make brick invisible
			var brick = '.row' + row + '.col' + col;
			$(brick).addClass( "broken" );


			if (speed1 == false)  // increase the speed as with each color of bricks
			{
				if (brick.indexOf("row7") >= 0)  // hit first green brick
				{
					defaultXVelocity *= 1.25;
					defaultYVelocity *= 1.25;
					paddleColor = 'lightgreen';
					$('#paddle').css('background-color', paddleColor);
					speed1 = true;
				}
			}
			else if (speed1 == true && speed2 == false)  // hit first yellow brick
			{
				if (brick.indexOf("row5") >= 0) 
				{
					defaultXVelocity *= 1.25;
					defaultYVelocity *= 1.25;
					paddleColor = '#f0f000';
					$('#paddle').css('background-color', paddleColor);
					speed2 = true;
				}
			}
			else if ( speed1 == true && speed2 == true && speed3 == false) // hit first orange brick
			{
				if (brick.indexOf("row3") >= 0) 
				{
					defaultXVelocity *= 1.25;
					defaultYVelocity *= 1.25;
					paddleColor = 'orange';
					$('#paddle').css('background-color', paddleColor);
					speed3 = true;
				}

			}
			else if ( speed1 == true && speed2 == true && speed3 == true && speed4 == false) // hit first red brick
			{
				if (brick.indexOf("row1") >= 0) 
				{
					defaultXVelocity *= 1.25;
					defaultYVelocity *= 1.25;
					paddleColor = 'indianred';
					$('#paddle').css('background-color', paddleColor);
					speed4 = true;
				}

			}
			else
			{
				//do not increase game speed;
			}
			

			// reverse y direction of ball
			if (vy > 0) //  force the ball down on brick collission
			{
				vy*=-1; 
			}
			var randomNum = Math.ceil(Math.random()*6); //keep it interesting by having a random chance of the ball switching horizontal postion
			if (randomNum > 4)
			{
				vx*=-1;
			}			
			by+20; // push the ball down when it hits a break
		}

	}
}




$(document).ready( function () 
{	
	for (row = 0; row < height; row++)
	{
		for (col = 0; col < width; col++)
		{
			$('#main').append('<div class="row' + row + ' col' + col + ' brick"></div>');
		}
	}; //sets the bricks on the screen

	// set the paddle
	$('#main').append('<div id="paddle"></div>');
	$('#paddle').css('left', (gameWidth - paddleWidth) / 2);
	$('#paddle').css('background-color', paddleColor);
	// Track the position of the mouse
	$('html').mousemove(function(e){
	        var x = e.clientX - $('#main')[0].offsetLeft - offset;
	        if (x <= leftEdge)
	        {
	        	x = 0;
	        }
	        else if (x >= rightEdge) 
	        {
		x = gameWidth - paddleWidth;
	        } 
	        else
	        {
	        }
	        $('#paddle').css({'left': x}); 	        
	    });

	//actiavte the ball with user click
	$('#main').one('click', function()
	{ 
		paddleColor = 'cyan';
		$('#paddle').css('background-color', paddleColor);
		// set the ball 
		$('#main').append('<div id="ball"></div>');
		$('#ball').css('left', 385);
		$('#ball').css('top', 530);
		// move the ball
		moveBall();
	});

});


	

